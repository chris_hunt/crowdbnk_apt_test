FROM crowdbnk/base:7

# Services setup
RUN a2dissite 000-default
COPY ./conf/site.conf /etc/apache2/sites-available/0000-site.conf
RUN a2ensite 0000-site

RUN mkdir /var/www/src 

COPY src /var/www/src

Run mkdir /var/www/src/Response/site/public/templates \
    && mkdir /var/www/src/Response/site/public/templates_c \
    && chmod -R 777 /var/www/src/Response/site/public/templates \
    && chmod -R 777 /var/www/src/Response/site/public/templates_c

WORKDIR /var/www/src/Response/site

# Install composer globally
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer && \
    composer install

