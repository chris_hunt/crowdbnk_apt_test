package calcs;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CopyOfDateUtils {
	
	public static double calculateTimeInterval(final Date d1, final Date d2) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		double timeInterval = 0;
		// Arrange Dates
		Date fromDate;
		Date toDate;
		if (d2.before(d1)) {
			fromDate = d2;
			toDate = d1;
		} else {
			fromDate = d1;
			toDate = d2;
		}

		Date earliestPaymentDate = earliestPaymentDate(fromDate, toDate);
		int[] monthsPeriod = null;
		int[] daysPeriod = null;
		if (null != earliestPaymentDate) {
			monthsPeriod = calculateNoofMonths(earliestPaymentDate, toDate);
			// System.out.println("MonthsPeriod->"+monthsPeriod[0]+"
			// "+monthsPeriod[1]);
			daysPeriod = calculateRemainingDays(fromDate, earliestPaymentDate);
			calculateTotalDays(earliestPaymentDate, daysPeriod);
			// System.out.println("DaysPeriod->"+daysPeriod[0]+"
			// "+daysPeriod[1]);

		} else {
			daysPeriod = calculateRemainingDays(fromDate, toDate);
			calculateTotalDays(fromDate, daysPeriod);
			// System.out.println("DaysPeriod->"+daysPeriod[0]+"
			// "+daysPeriod[1]);
		}
		// System.out.println("...."+monthsPeriod);
		if (null != monthsPeriod) {
			timeInterval = (monthsPeriod[0] / (double) monthsPeriod[1]) + (daysPeriod[0] / (double) daysPeriod[1]);
		} else {
			timeInterval = daysPeriod[0] / (double) daysPeriod[1];
		}

		return timeInterval;
	}

	private static void calculateTotalDays(Date inputDate, int[] daysPeriod) {
		Date inputDateVar = new Date(inputDate.getYear(), inputDate.getMonth(), inputDate.getDate());

		Date lastYearSameDate = new Date(inputDate.getYear() - 1, inputDate.getMonth(), inputDate.getDate());

		int totalDays = 0;

		boolean isCurrentYearLeapYear = false;
		boolean isPrevYearLeapYear = false;

		// Check Current Year is Leap Year

		int currentYearFebDays = 0;
		int prevYearFebDays = 0;
		if ((inputDate.getYear() % 400 == 0) || ((inputDate.getYear() % 4 == 0) && (inputDate.getYear() % 100 != 0))) {
			currentYearFebDays = 29;

			isCurrentYearLeapYear = true;
		} else {
			currentYearFebDays = 28;

		}
		if ((lastYearSameDate.getYear() % 400 == 0)
				|| ((lastYearSameDate.getYear() % 4 == 0) && (lastYearSameDate.getYear() % 100 != 0))) {
			isPrevYearLeapYear = true;

			prevYearFebDays = 29;
		} else {

			prevYearFebDays = 28;
		}

		final int FEBRUARY = 1;
		Date currentYearFebDate = new Date(inputDate.getYear(), FEBRUARY, currentYearFebDays);
		Date prevYearFebDate = new Date(inputDate.getYear() - 1, FEBRUARY, prevYearFebDays);

		// If Earliest Payment Date falls on or after currentYearFebDate OR If
		// Earliest Payment Date falls on or before prevYearFebDate
		if ((isCurrentYearLeapYear && inputDateVar.compareTo(currentYearFebDate) >= 0)
				|| (isPrevYearLeapYear && lastYearSameDate.compareTo(prevYearFebDate) <= 0)) {
			totalDays = 366;
		} else {
			totalDays = 365;
		}
		daysPeriod[1] = totalDays;
	}

	private static int[] calculateRemainingDays(Date d1, Date d2) {

		int[] monthDay = new int[] { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int[] period = new int[4];
		int preMonthDays = 0;
		int postMonthDays = 0;
		int noOfDays = 0;
		Date lastDateInPrevYear = null;
		if (d1.getMonth() == d2.getMonth()) {
			noOfDays = d2.getDate() - d1.getDate();
		} else {
			if ((d1.getMonth() == 1)) {
				// Month is Feb.Check for Leap Year
				if ((d1.getYear() % 400 == 0) || ((d1.getYear() % 4 == 0) && (d1.getYear() % 100 != 0))) {
					preMonthDays = 29 - d1.getDate();
				} else {
					preMonthDays = 28 - d1.getDate();
				}
			} else {
				preMonthDays = monthDay[d1.getMonth()] - d1.getDate();
			}

			// Post Month Days
			postMonthDays = d2.getDate();
			noOfDays = preMonthDays + postMonthDays;
		}
		period[0] = noOfDays;

		return period;

	}

	private static int[] calculateNoofMonths(Date earliestPaymentDate, Date toDate) {
		int[] period = new int[4];
		int noOfYears = toDate.getYear() - earliestPaymentDate.getYear();
		int monthAdj = earliestPaymentDate.getMonth() + 1 + 12 - (toDate.getMonth() + 1);
		int noOfMonths = (noOfYears + 1) * 12 - monthAdj;
		period[0] = noOfMonths;
		period[1] = 12;
		// TODO Auto-generated method stub
		return period;

	}

	private static Date earliestPaymentDate(Date fromDate, Date toDate) {
		Date returnDate = null;
		// Prepare Data
		int[] monthDay = new int[] { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int edd = fromDate.getDate();
		int emm = fromDate.getMonth();
		int eyy = fromDate.getYear();

		int ldd = toDate.getDate();
		int lmm = toDate.getMonth();
		int lyy = toDate.getYear();

		int earliestPymtMonth = 0;
		int earliestPymtDate = 0;
		Date earliestDate = null;
		if ((fromDate.getYear() % 400 == 0) || ((fromDate.getYear() % 4 == 0) && (fromDate.getYear() % 100 != 0))) {
			monthDay[1] = 29;
		} else {
			monthDay[1] = 28;
		}
		// Check if Early Date cone first bed- Calendar Wise
		if (ldd >= edd) {

			if (ldd <= monthDay[emm]) {
				earliestPymtDate = ldd;
				earliestPymtMonth = emm;

			} else {
				earliestPymtDate = monthDay[emm];
				earliestPymtMonth = emm;

			}
		} else {
			earliestPymtDate = ldd;
			earliestPymtMonth = emm + 1;
		}
		earliestDate = new Date(eyy, earliestPymtMonth, earliestPymtDate);
		if (earliestDate.before(toDate)) {
			returnDate = earliestDate;
		}
		return returnDate;

	}
}
