<?php

class CopyOfDateUtils {

	public function calculateTimeInterval($d1, $d2) {

		$timeInterval = 0;
		// Arrange Dates		
		if ($d2 < $d1) {
			$fromDate = $d2;
			$toDate = $d1;
		} else {
			$fromDate = $d1;
			$toDate = $d2;
		}

		$earliestPaymentDate = $this->earliestPaymentDate($fromDate, $toDate);
		$monthsPeriod = NULL;
		$daysPeriod = NULL;

		if (NULL != $earliestPaymentDate) {			
			$monthsPeriod = $this->calculateNoofMonths($earliestPaymentDate, $toDate);
			$daysPeriod = $this->calculateRemainingDays($fromDate, $earliestPaymentDate);
			$daysPeriod = $this->calculateTotalDays($earliestPaymentDate, $daysPeriod);
		} else {
			$daysPeriod = $this->calculateRemainingDays($fromDate, $toDate);					
			$daysPeriod = $this->calculateTotalDays($fromDate, $daysPeriod);
		}

		if (NULL != $monthsPeriod) {
			$timeInterval = ($monthsPeriod[0] / (double) $monthsPeriod[1]) + ($daysPeriod[0] / (double) $daysPeriod[1]);
		} else {
			$timeInterval = $daysPeriod[0] / (double) $daysPeriod[1];
		}
		return $timeInterval;
	}

	private function calculateTotalDays($inputDate, $daysPeriod) {
		$t = getdate($inputDate);
		$tdd = $t["mday"];
		$tmm = $t["mon"];
		$tyy = $t["year"];
		
		$lastYearSameDate = strtotime($tyy - 1 . "-" . $tmm . "-" . $tdd); 

		$totalDays = 0;

		$isCurrentYearLeapYear = FALSE;
		$isPrevYearLeapYear = FALSE;

		// Check Current Year is Leap Year

		$currentYearFebDays = 0;
		$prevYearFebDays = 0;
		if (($tyy % 400 == 0) || (($tyy % 4 == 0) && (tyy % 100 != 0))) {
			$currentYearFebDays = 29;
			$isCurrentYearLeapYear = TRUE;
		} else {
			$currentYearFebDays = 28;
		}
		
		$l = getdate($lastYearSameDate);
		$ldd = $t["mday"];
		$lmm = $t["mon"];
		$lyy = $t["year"];
		
		if (($lyy % 400 == 0) || (($lyy % 4 == 0) && ($lyy % 100 != 0))) {
			$isPrevYearLeapYear = TRUE;
			$prevYearFebDays = 29;
		} else {
			$prevYearFebDays = 28;
		}

		$FEBRUARY = 1;
		$currentYearFebDate = strtotime($tyy . "-02-" . $currentYearFebDays);
		$prevYearFebDate = strtotime($tyy - 1 . "-02-" . $prevYearFebDays);

		// If Earliest Payment Date falls on or after currentYearFebDate OR If
		// Earliest Payment Date falls on or before prevYearFebDate
		if (($isCurrentYearLeapYear && $inputDate > $currentYearFebDate) 
				|| ($isPrevYearLeapYear && $lastYearSameDate < $prevYearFebDate)) { 
			$totalDays = 366;
		} else {
			$totalDays = 365;
		}
		$daysPeriod[1] = $totalDays;
		
		return $daysPeriod;
	}

	private function calculateRemainingDays($d1, $d2) {

		$monthDay = array( 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
		$period = array( 0, 0, 0,  0);
		$preMonthDays = 0;
		$postMonthDays = 0;
		$noOfDays = 0;
		$lastDateInPrevYear = NULL;
		
		$d1d = getdate($d1);
		$d1dd = $d1d["mday"];
		$d1mm = $d1d["mon"];
		$d1yy = $d1d["year"];
		
		$d2d = getdate($d2);
		$d2dd = $d2d["mday"];
		$d2mm = $d2d["mon"];
		$d2yy = $d2d["year"];

		if ($d1mm == $d2mm && $d1yy == $d2yy) {
			$noOfDays = $d2dd - $d1dd;
		} else {
			if (($d1mm == 2)) {
				// Month is Feb.Check for Leap Year
				if (($d1yy % 400 == 0) || (($d1yy % 4 == 0) && ($d1yy % 100 != 0))) {
					$preMonthDays = 29 - $d1dd;
				} else {
					$preMonthDays = 28 - $d1dd;
				}
			} else {				
				$preMonthDays = $monthDay[$d1mm - 1] - $d1dd;
			}
			
			// Post Month Days
			$postMonthDays = $d2dd;
			$noOfDays = $preMonthDays + $postMonthDays;

		}
		$period[0] = $noOfDays;

		return $period;

	}

	private function calculateNoofMonths($earliestPaymentDate, $toDate) {
		$period = array(0, 0, 0, 0);

		$e = getdate($earliestPaymentDate);
		$edd = $e["mday"];
		$emm = $e["mon"];
		$eyy = $e["year"];
		
		$t = getdate($toDate);
		$tdd = $t["mday"];
		$tmm = $t["mon"];
		$tyy = $t["year"];
		
		$noOfYears = $tyy - $eyy;
		$monthAdj = $emm + 1 + 12 - ($tmm + 1);
		$noOfMonths = ($noOfYears + 1) * 12 - $monthAdj;
		$period[0] = $noOfMonths;
		$period[1] = 12;

		return $period;

	}

	private function earliestPaymentDate($fromDate, $toDate) {
		$returnDate = NULL;
		
		// Prepare Data
		$monthDay = array( 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
		$e = getdate($fromDate);
		$edd = $e["mday"];
		$emm = $e["mon"];
		$eyy = $e["year"];

		$l = getdate($toDate);
		$ldd = $l["mday"];
		$lmm = $l["mon"];
		$lyy = $l["year"];

		$earliestPymtMonth = 0;
		$earliestPymtDate = 0;
		$earliestDate = NULL;
		if (($eyy % 400 == 0) || (($eyy % 4 == 0) && ($eyy % 100 != 0))) {
			$monthDay[1] = 29;
		} else {
			$monthDay[1] = 28;
		}
		// Check if Early Date cone first bed- Calendar Wise
		if ($ldd >= $edd) {

			if ($ldd <= $monthDay[$emm - 1]) {
				$earliestPymtDate = $ldd;
				$earliestPymtMonth = $emm;
			} else {
				$earliestPymtDate = $monthDay[$emm - 1];
				$earliestPymtMonth = $emm;

			}
		} else {
			$earliestPymtDate = $ldd;
			if ($emm == 12) {
				$eyy ++;
				$earliestPymtMonth = 1;
			} else {
				$earliestPymtMonth = $emm + 1;	
			}
			
		}
		$earliestDate = strtotime($earliestPymtDate . "-" . $earliestPymtMonth . "-" . $eyy);

		if ($earliestDate < $toDate) {
			$returnDate = $earliestDate;
		}
		return $returnDate;

	}
}



?>
