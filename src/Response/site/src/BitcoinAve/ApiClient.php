<?php 

namespace BitcoinAve;

class ApiClient
{
	private $guzzle;
	
	public function __construct(\GuzzleHttp\Client $guzzle) {
		$this->guzzle = $guzzle;
	}
	
	public function getHistory($currency, $endpoint) 
	{
		$res = $this->guzzle->request('GET', "https://api.bitcoinaverage.com/history/$currency/$endpoint.csv");
		return $res->getBody();
	}	
}