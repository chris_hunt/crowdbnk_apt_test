<?php 

namespace BitcoinAve;

class HistoryConverter 
{
	private $apiClient;
	
	public function __construct($apiClient) 
	{
		$this->apiClient = $apiClient;
	}
	
	public function getHistory($currency, $endpoint)
	{
		$dataCSV = explode("\r\n", $this->apiClient->getHistory($currency, $endpoint));		
		$headings = [];
		$data = [];
		$count = 0;
		
		foreach ($dataCSV as $row) {
			$rowdata = explode(",", $row);
			if ($count == 0) {
				$headings = $rowdata;
			} else {
				$newrow = [];
				foreach ($rowdata as $key=>$value) {
					$newrow[$headings[$key]] = $value;
				}
				$data[] = $newrow;
			}
			$count ++;
		}		
		return $data;	
	}	
}


?>