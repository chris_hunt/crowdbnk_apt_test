<?php

class CopyOfDateUtils
{
    CONST MONTHDAYS = array(31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    CONST FEBDAYS = 28;
    CONST FEBLEAPDAYS = 29;
    CONST MONTHSINYEAR = 12;
    CONST DAYSINYEAR = 365;
    CONST DAYSINLEAPYEAR = 366;

    public function calculateTimeInterval($d1, $d2)
    {
        $fromDate = new \DateTime();
        $toDate = new \DateTime();

        $timeInterval = 0;
        // Arrange Dates		
        if ($d2 < $d1) {
            $fromDate->setTimestamp($d2);
            $toDate->setTimestamp($d1);
        } else {
            $fromDate->setTimestamp($d1);
            $toDate->setTimestamp($d2);
        }

        $earliestPaymentDate = $this->earliestPaymentDate($fromDate, $toDate);
        $monthsPeriod = null;
        $daysPeriod = null;

        if (null != $earliestPaymentDate) {
            $monthsPeriod = $this->calculateNoofMonths($earliestPaymentDate, $toDate);
            $daysPeriod = $this->calculateRemainingDays($fromDate, $earliestPaymentDate);
            $daysPeriod = $this->calculateTotalDays($earliestPaymentDate, $daysPeriod);
        } else {
            $daysPeriod = $this->calculateRemainingDays($fromDate, $toDate);
            $daysPeriod = $this->calculateTotalDays($fromDate, $daysPeriod);
        }

        if (null != $monthsPeriod) {
            $timeInterval = ($monthsPeriod[0] / (double) $monthsPeriod[1]) + ($daysPeriod[0] / (double) $daysPeriod[1]);
        } else {
            $timeInterval = $daysPeriod[0] / (double) $daysPeriod[1];
        }

        return $timeInterval;
    }

    private function calculateTotalDays($inputDate, $daysPeriod)
    {
        $tdd = $inputDate->format('j');
        $tmm = $inputDate->format('n');
        $tyy = $inputDate->format('Y');

        $lastYearSameDate = DateTime::createFromFormat('Y-n-j', $tyy - 1 .'-'.$tmm.'-'.$tdd);
        $totalDays = 0;

        $isCurrentYearLeapYear = false;
        $isPrevYearLeapYear = false;

        // Check Current Year is Leap Year
        $currentYearFebDays = 0;
        $prevYearFebDays = 0;
        if ($this->isLeapYear($tyy)) {
            $currentYearFebDays = self::FEBLEAPDAYS;
            $isCurrentYearLeapYear = true;
        } else {
            $currentYearFebDays = self::FEBDAYS;
        }

        $ldd = $lastYearSameDate->format('j');
        $lmm = $lastYearSameDate->format('n');
        $lyy = $lastYearSameDate->format('Y');

        if ($this->isLeapYear($lyy)) {
            $isPrevYearLeapYear = true;
            $prevYearFebDays = self::FEBLEAPDAYS;
        } else {
            $prevYearFebDays = self::FEBDAYS;
        }

        $FEBRUARY = 1;
        $currentYearFebDate = DateTime::createFromFormat('j-n-Y', $tyy.'-02-'.$currentYearFebDays);
        $prevYearFebDate = DateTime::createFromFormat('j-n-Y', $tyy - 1 .'-02-'.$prevYearFebDays);

        // If Earliest Payment Date falls on or after currentYearFebDate OR If
        // Earliest Payment Date falls on or before prevYearFebDate
        if (($isCurrentYearLeapYear && $inputDate > $currentYearFebDate)
                || ($isPrevYearLeapYear && $lastYearSameDate < $prevYearFebDate)) {
            $totalDays = self::DAYSINLEAPYEAR;
        } else {
            $totalDays = self::DAYSINYEAR;
        }
        $daysPeriod[1] = $totalDays;

        return $daysPeriod;
    }

    private function calculateRemainingDays($d1, $d2)
    {
        $monthDay = self::MONTHDAYS;
        $period = array(0, 0, 0,  0);
        $preMonthDays = 0;
        $postMonthDays = 0;
        $noOfDays = 0;
        $lastDateInPrevYear = null;

        $d1dd = $d1->format('j');
        $d1mm = $d1->format('n');
        $d1yy = $d1->format('Y');

        $d2dd = $d2->format('j');
        $d2mm = $d2->format('n');
        $d2yy = $d2->format('Y');

        if ($d1mm == $d2mm && $d1yy == $d2yy) {
            $noOfDays = $d2dd - $d1dd;
        } else {
            if (($d1mm == 2)) {
                // Month is Feb.Check for Leap Year
                if ($this->isLeapYear($d1yy)) {
                    $preMonthDays = self::FEBLEAPDAYS - $d1dd;
                } else {
                    $preMonthDays = self::FEBDAYS - $d1dd;
                }
            } else {
                $preMonthDays = $monthDay[$d1mm - 1] - $d1dd;
            }

            // Post Month Days
            $postMonthDays = $d2dd;
            $noOfDays = $preMonthDays + $postMonthDays;
        }
        $period[0] = $noOfDays;

        return $period;
    }

    private function calculateNoofMonths($earliestPaymentDate, $toDate)
    {
        $period = array(0, 0, 0, 0);

        $edd = $earliestPaymentDate->format('j');
        $emm = $earliestPaymentDate->format('n');
        $eyy = $earliestPaymentDate->format('Y');

        $tdd = $toDate->format('j');
        $tmm = $toDate->format('n');
        $tyy = $toDate->format('Y');

        $noOfYears = $tyy - $eyy;
        $monthAdj = $emm + 1 + 12 - ($tmm + 1);
        $noOfMonths = ($noOfYears + 1) * 12 - $monthAdj;
        $period[0] = $noOfMonths;
        $period[1] = self::MONTHSINYEAR;

        return $period;
    }

    private function earliestPaymentDate($fromDate, $toDate)
    {
        $returnDate = null;

        // Prepare Data
        $monthDay = self::MONTHDAYS;

        $edd = $fromDate->format('j');
        $emm = $fromDate->format('n');
        $eyy = $fromDate->format('Y');

        $ldd = $toDate->format('j');
        $lmm = $toDate->format('n');
        $lyy = $toDate->format('Y');

        $earliestPymtMonth = 0;
        $earliestPymtDate = 0;
        $earliestDate = null;
        if ($this->isLeapYear($eyy)) {
            $monthDay[1] = self::FEBLEAPDAYS;
        } else {
            $monthDay[1] = self::FEBDAYS;
        }
        // Check if Early Date cone first bed- Calendar Wise
        if ($ldd >= $edd) {
            if ($ldd <= $monthDay[$emm - 1]) {
                $earliestPymtDate = $ldd;
                $earliestPymtMonth = $emm;
            } else {
                $earliestPymtDate = $monthDay[$emm - 1];
                $earliestPymtMonth = $emm;
            }
        } else {
            $earliestPymtDate = $ldd;
            if ($emm == 12) {
                ++$eyy;
                $earliestPymtMonth = 1;
            } else {
                $earliestPymtMonth = $emm + 1;
            }
        }
        $earliestDate = DateTime::createFromFormat('j-n-Y', $earliestPymtDate.'-'.$earliestPymtMonth.'-'.$eyy);
        if ($earliestDate < $toDate) {
            $returnDate = $earliestDate;
        }

        return $returnDate;
    }

    private function isLeapYear($year)
    {
        return ($year % 400 == 0) || (($year % 4 == 0) && ($year % 100 != 0));
    }
}
