<?php
require_once __DIR__ ."/./CopyOfDateUtils.php";

$csvdata = __DIR__. "/../../../../Q2 Code Rectification/testdata.csv";
?>
<table>
	<tr>
		<th>Day 1</th>
		<th>Month 1</th>
		<th>Year 1</th>
		<th>Day 2</th>
		<th>Month 2</th>
		<th>Year 2</th>
		<th>Months diff</th>
		<th>Days diff</th>
		<th>Days in year</th>
		<th>Period from class</th>
		<th>Period calculated</th>
	</tr>
<?php
$row = 1;
if (($handle = fopen($csvdata, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
    	if ($row > 1) {
    		$date1 = new DateTime();
			$date1->setDate($data[2], $data[1], $data[0])->setTime(0,0,0);
			$date2 = new DateTime();
			$date2->setDate($data[5], $data[4], $data[3])->setTime(0,0,0);

			$dateutil = new CopyOfDateUtils();

    		echo "<tr><td>" . $data[0] . "</td><td>" . $data[1] . "</td><td>" . $data[2] . "</td>
    		<td>" . $data[3] . "</td><td>" . $data[4] . "</td><td>" . $data[5] . "</td>
    		<td>" . $data[6] . "</td>
    		<td>" . $data[7] . "</td>
    		<td>" . $data[8] . "</td>
			<td>" . $dateutil->calculateTimeInterval($date1->getTimeStamp(), $date2->getTimeStamp()) . "</td>
			<td>" . (($data[6] / 12) + ($data[7] / $data[8])) . "</td>
    		</tr>";
    	}
    	$row ++;
    }
    fclose($handle);
}
?>
</table>