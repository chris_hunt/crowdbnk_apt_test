<?php
require __DIR__ . "/../vendor/autoload.php";
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Set templating engine
$smarty = new Smarty();
$smarty->setTemplateDir('../template');
$smarty->setCompileDir('../templates_c');

// Handle response
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
$app->get('/api/history/{currencycode}/{data}', function (Request $request, Response $response) {
	$currencyCode = $request->getAttribute('currencycode');
	$data = $request->getAttribute('data');
	$guzzle = new \GuzzleHttp\Client();	
	$apiClient = new \BitcoinAve\ApiClient($guzzle);	
	$conv = new \BitcoinAve\HistoryConverter($apiClient);	
	$response->getBody()->write(
		json_encode($conv->getHistory($currencyCode, $data))
	);	
	$response = $response->withHeader('Content-Type', 'application/json');
	return $response;
});

$app->get('/charts', function (Request $request, Response $response) use ($smarty) {
	$content = $smarty->fetch('charts.tpl');
	$smarty->assign('content', $content);
	$smarty->assign('footer', '<script src="js/main.js"></script>');
	$response->getBody()->write($smarty->fetch('main.tpl'));
	return $response;
});

$app->get('/summary', function (Request $request, Response $response) use ($smarty) {
	$content = $smarty->fetch('summary.tpl');
	$smarty->assign('content', $content);
	$response->getBody()->write($smarty->fetch('main.tpl'));
	return $response;
});

$app->run();

