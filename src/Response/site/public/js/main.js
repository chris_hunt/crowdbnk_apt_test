loadCurrencies();
google.charts.load('current', {
	'packages' : [ 'corechart' ]
});
google.charts.setOnLoadCallback(drawChart);

function loadCurrencies() {
	$.ajax({
		url : 'https://api.bitcoinaverage.com/history/'
	}).done(
			function(data) {
				$.each(data, function(index, row) {
					$('#currency-select').append(
							$("<option></option>").attr("value", index).text(
									index));
				});
				$('#currency-select').val('GBP');
			});
}

$(document).ready(function() {	
	$('#currency-select').on('change', function() {
		drawChart();
	});
	$('#period-select').on('change', function() {
		drawChart();
	});	
	$('#interval-select').on('change', function() {
		drawChart();
	});	
});

var interval;
function drawChart() {
	clearInterval(interval);
	var intervalTime = $("#interval-select").val();
	getDataDraw($('#currency-select').val(), $('#period-select').val());
	if (intervalTime > 0) {
		interval = setInterval(function() {
			getDataDraw($('#currency-select').val(), $('#period-select').val());
		}, intervalTime);
	}
}

function getDataDraw(currency, period) {
	var file = '';
	switch (period) {
	case "all":
		file = 'per_day_all_time_history';
		break;
	case "month":
		file = 'per_hour_monthly_sliding_window';
		break;
	case "day":
		file = 'per_minute_24h_sliding_window';
		break;
	}
	$.ajax({
		url : "/api/history/" + currency + "/" + file
	}).done(
			function(data) {
				var chartdata = new google.visualization.DataTable();
				chartdata.addColumn('datetime', 'Date');
				chartdata.addColumn('number', 'Price');
				$.each(data, function(index, row) {
					var rowdate;
					if (period == 'all') {
						rowdate = new Date(row.datetime.substr(0, 4),
								parseInt(row.datetime.substr(5, 2)) - 1,
								row.datetime.substr(8, 2));
					} else {
						rowdate = new Date(row.datetime.substr(0, 4),
								parseInt(row.datetime.substr(5, 2)) - 1,
								row.datetime.substr(8, 2), row.datetime.substr(
										11, 2), row.datetime.substr(14, 2),
										row.datetime.substr(17, 2));
					}

					chartdata.addRow([ rowdate, parseFloat(row.average) ]);
				});

				var options = {
					title : 'Bitcoin price - ' + currency,
					legend : {
						position : 'bottom'
					}
				};

				var chart = new google.visualization.LineChart(document
						.getElementById('chart'));

				chart.draw(chartdata, options);
			});
}