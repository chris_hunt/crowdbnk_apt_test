
		<div class="row">
			<div class='col-xs-12 col-sm-6 col-lg-3'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">General</h3>
					</div>
					<div class="panel-body">
						<p>
							All work contained in <a
								href='https://bitbucket.org/chris_hunt/crowdbnk_apt_test'
								target='_new'>Bitbucket repo</a>.
						</p>
						<p>
							Time taken for entire aptitude test was approximately 5.5 hours.
						</p>
					</div>
				</div>
			</div>

			<div class='col-xs-12 col-sm-6 col-lg-3'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Q1</h3>
					</div>
					<div class="panel-body">
						<h4>Task A</h4>
						<p>
							Response at <a target='_new' href='/Q1/phprev.txt'>phprev.txt</a>
						</p>
						<h4>Task B</h4>
						<p>
							I don't know Java so I can only review with a general coding observations.
						</p>
						<p>
							Response at <a target='_new' href='/Q1/javarev.txt'>javarev.txt</a>
						</p>
					</div>
				</div>
			</div>
			<div class='col-xs-12 col-sm-6 col-lg-3'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Q2</h3>
					</div>
					<div class="panel-body">
						<h4>Task A</h4>
						<p>I created a simple test script which ran in the test file
							provided (<a href='/Q2/Q2_Task_A_test.php' target='_new'>Q2_task_A_test.php</a>).</p>
						<p>It puts the dates through the method to be fixed and compares
							with a simple calculation using the month and day values also
							provided. It was noticable that issues occurred where earliest payment month was in December.
						</p>
						<p>Created a git repo and added the initial files.</p>
						<p>Amended code and ensured that all tests passed.</p>
						<p>I would provide a diff to the development team (<a target='_new' href='https://bitbucket.org/chris_hunt/crowdbnk_apt_test/diff/Q1%20Code%20Review/CopyOfDateUtils.php?diff1=a399b57d324511b0d16724b82007fbec62d39f36&diff2=ba96b1e5bf9e&at=master'>click for diff</a>) from the new version of the
							class.</p>
						<p>I'd pass the test script and the diff back to the team to apply
							the change.</p>
						<p>I'd encourage all new methods to be specced with a suitable tool. I prefer PHPSpec.</p>
						

						<h4>Task B</h4>
						<p>This date utils class is more a standalone method than a class. On the basis that it now works following the bug fix, I'd be reluctant to rewrite from scratch.</p>
						<p>I would make it clear that this class should not be added to but if more date utility methods are required, a wrapper class should be created which calls this class and method.</p>
						<p>The few changes I would make to this class would be 
							<ul>
								<li>to standardise the use of date classes. I'd convert to standard \DateTime throughout. Dates which are explicitly built from strings should provide a format so that incorrectly created strings produce an exception.</li>
								<li>to extract repeated code</li>
								<li>to make the code PSR-0 compliant</li>
								<li>to extract constants from code</li>
							</ul>
						</p>
						<p>The final class is CopyOfDateUtils.php in <a target="_new" href='https://bitbucket.org/chris_hunt/crowdbnk_apt_test/src//Response/site/public/Q2/?at=master'>Bitbucket</a></p>
						<p>Tests were maintained against the new class (<a href='/Q2/Q2_Task_B_test.php'>Q2_Task_B_test.php</a>.)
						<p>Generally, I would have written specs for the new methods I've introduced using PHPSpec and would write these against the test data provided.</p>
					</div>
				</div>
			</div>
			<div class='col-xs-12 col-sm-6 col-lg-3'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Q3</h3>
					</div>
					<div class="panel-body">
						<h4>Task A</h4>

						<p>API produced in the same format as the BitcoinAverage site - /api/history/<i>currency</i>/<i>type</i>
						</p>
						<p>Examples:<ul>
							<li><a target='_new' href='/api/history/GBP/per_day_all_time_history'>/api/history/GBP/per_day_all_time_history</a></li>
							<li><a target='_new' href='/api/history/GBP/per_minute_24h_sliding_window'>/api/history/GBP/per_minute_24h_sliding_window</a></li>
							<li><a target='_new' href='/api/history/GBP/per_minute_24h_global_average_sliding_window'>/api/history/GBP/per_minute_24h_global_average_sliding_window</a></li>
							<li><a target='_new' href='/api/history/GBP/per_hour_monthly_sliding_window'>/api/history/GBP/per_hour_monthly_sliding_window</a></li>
							<li><a target='_new' href='/api/history/GBP/volumes'>/api/history/GBP/volumes</a></li>
						</ul></p>
						
						<p>
							Classes in <a target="_new" href='https://bitbucket.org/chris_hunt/crowdbnk_apt_test/src//Response/site/src/BitcoinAve/?at=master'>Bitbucket</a>
						</p>

						<h4>Task B</h4>
						<p><a href='/charts'>Click for Bitcoin prices UI</a></p>
						<p>UI is entirely HTML and JS. The JS can be found <a target='_new' href='/js/main.js'>here</a>.</p>
					</div>
				</div>
			</div>
		</div>

