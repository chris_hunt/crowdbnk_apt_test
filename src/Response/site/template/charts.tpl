
		<div id='presentation' class="row">
			<div class='col-sm-3'>
				<div class="panel panel-default">
					<div class="panel-body">
						<form>
							<div class='form-group'>
								<label for="currency-select">Select currency</label> <select
									id='currency-select' class="form-control">
								</select>
							</div>
							<div class='form-group'>
								<label for="period-select">Select period</label> <select
									id='period-select' class="form-control">
									<option value='all'>All time</option>
									<option value='month'>Month</option>
									<option value='day'>Day</option>
								</select>
							</div>
							<div class='form-group'>
								<label for="interval-select">Select refresh</label> <select
									id='interval-select' class="form-control">
									<option value='0'>Never</option>
									<option value='10000'>10 seconds</option>
									<option value='60000' selected>1 minute</option>
									<option value='300000'>5 minutes</option>
								</select>
							</div>
						</form>
					</div>
				</div>

			</div>
			<div class='col-sm-9'>
				<div id="chart"></div>
			</div>

		</div>

