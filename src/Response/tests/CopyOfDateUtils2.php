<?php

class CopyOfDateUtils2
{
	public function calculateTimeInterval($d1, $d2) 
	{
		$timeInterval = 0;
		// Arrange Dates
		$fromDate = new DateTime();
		$toDate = new DateTime();
		if (strtotime($d2) < strtotime($d1)) {
			$fromDate->setTimestamp($d2);
			$toDate->setTimestamp($d1);
		} else {
			$fromDate->setTimestamp($d1);
			$toDate->setTimestamp($d2);
		}

		$monthDiff = $this->getMonthDiff($fromDate, $toDate);
		
		return $monthDiff; //$this->calculateInterval($monthDiff, 12, $daysDiff, $daysInYear);
	}
	
	private function getMonthDiff($fromDate, $toDate) 
	{
		$monthDiff = 0;
		$testFromDate = new DateTime();
		$testToDate = new DateTime();
		
		$testFromDate->setDate($fromDate->format("Y"), $fromDate->format("n"), 1);
		$testToDate->setDate($toDate->format("Y"), $toDate->format("n"), 1);
		
		
		while (true) {			
			$testFromDate->add(new DateInterval('P1M'));
			if ($testFromDate > $testToDate) {
				break;
			}
			$monthDiff ++;			
		}
		return $monthDiff;
	}
	
	private function calculateInterval($monthDiff, $monthsInYear, $daysDiff, $daysInYear)
	{
		return ($monthDiff / $monthsInYear) + ($daysDiff / $daysInYear);
	}
	
}